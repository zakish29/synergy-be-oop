public class Anggota extends ClassBE{
    @Override
    public String m_judul(){
        return "ini method dari class Anggota";
    }

    @Override
    public String m_anggota(String nama, int umur){
        this.nama = nama;
        this.umur = umur;

        return "nama saya adalah " + getNama() + " dan saya " + getUmur() + " tahun.";
    }

    public String m_anggota(int umur, String nama){
        this.umur = umur;
        this.nama = nama;

        return "nama saya adalah " + getNama() + " dan saya " + getUmur() + " tahun.";
    }

}
