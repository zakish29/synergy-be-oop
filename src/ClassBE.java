public class ClassBE {
    String nama;
    int umur;

    public String m_judul(){
        return "ClassBE Method final anggota";
    }

    public String m_anggota(String nama, int umur){
        this.nama=nama;
        this.umur=umur;

        System.out.println("Method m_anggota pada ClassBE");

        return "nama saya adalah "+ getNama() + " dan saya " + getUmur() + " tahun";
    }

    public String getNama(){
        return nama;
    }

    public void SetNama(String nama){
        this.nama = nama;
    }

    public int getUmur(){
        return  umur;
    }

    public void setUmur(int umur){
        this.umur = umur;
    }
}
