public class MainCode {

    public static void main(String[] args){
        // b
        ClassBE cbe = new ClassBE();
        // c
        cbe.m_judul();
        // d
        System.out.println(cbe.m_anggota("Ani", 65));
        // e
        Anggota agt = new Anggota();
        // f
        System.out.println(agt.m_anggota("Trump", 34));
        // g
        System.out.println(agt.m_judul());
        // h
        System.out.println(agt.m_anggota(22, "Ari"));

    }
}
